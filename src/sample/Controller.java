package sample;

import com.sun.media.jfxmedia.events.PlayerTimeListener;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public Button btnWorking;
    public ProgressBar pgWorking;
    public ImageView iv;

    public void OnWorking(ActionEvent actionEvent) {
        List<String> imagepath = new ArrayList<>();

        for(int i = 0 ; i < 4 ; i++){
            imagepath.add("/resourses/".concat(String.valueOf(i)).concat(".png"));
        }

        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for(int i = 0 ; i <= 4 ; i++) {
            int j = i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(imagepath.get(j)).toString()));
                        }
                    })
            );
        }


        Task task = new Task<Void>(){

            @Override
            protected Void call() throws Exception {
                final int max = 100;
                pgWorking.setVisible(true);

                for (int i = 1 ; i < max ; i++){
                    Thread.sleep(35);
                    updateProgress(i, max);
                }
                return null;
            }

            @Override
            protected void scheduled() {
                super.scheduled();
                pgWorking.setVisible(true);
                iv.setVisible(true);
                timeline.play();
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                pgWorking.setVisible(false);
                iv.setVisible(false);
               //btnWorking.setVisible(false);
                timeline.stop();
            }
        };
        pgWorking.progressProperty().bind(task.progressProperty());
        new Thread(task).start();


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pgWorking.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        pgWorking.setVisible(false);
    }
}
